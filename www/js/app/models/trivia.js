define(['app/settings', 'backbone', './base'], function(Settings, Backbone, Base)
{
	"use strict";

	var questionModel = Base.Model.extend();
	
	var questionCollection = Base.EventChildrenCollection.extend({
		urlRoot: Settings.resourceUriFor('trivia/question'),
		model: questionModel,
		initialize: function(models, options)
		{
			this.groupId = options.groupId;
		},
		url: function(models)
		{
			var base = Base.EventChildrenCollection.prototype.url.apply(this, models);
			return base + '&group_pk=' + this.groupId;
		},

	});

	var groupCollection = Base.EventChildrenCollection.extend({
		urlRoot: Settings.resourceUriFor('trivia/question-group'),
	}); 

	return {
		QuestionModel: questionModel,
		QuestionCollection: questionCollection,
		GroupCollection: groupCollection
	};
});