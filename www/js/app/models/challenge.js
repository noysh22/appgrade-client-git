define(['app/settings', 'backbone', 'app/helpers/string', './base'], function(Settings, Backbone, StringHelper, Base)
{
	"use strict";

	var challengeModel = Base.Model.extend();
	
	var challengeCollection = Base.EventChildrenCollection.extend({
		urlRoot: Settings.resourceUriFor('challenge'),
		model: challengeModel,
		initialize: function(models, options)
		{
			this.classification = options.classification;
			// this.listenTo('sync', this.filter());
		},
		url: function(models)
		{
			var base = Base.EventChildrenCollection.prototype.url.apply(this, models);
			return '{0}&classification={1}'.format(base, this.classification);
		}
	});

	// var challengeGroupCollection = Base.Collection.extend({
	// 	urlRoot: Settings.resourceUriFor('trivia/question-group'),
	// }); 

	return {
		ChallengeModel: challengeModel,
		ChallengeCollection: challengeCollection,
		// ChallengeGroupCollection: challengeGroupCollection
	};
});