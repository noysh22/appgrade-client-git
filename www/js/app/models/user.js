define(['app/settings', './base'], function (Settings, Base) {
	"use strict";

	return Base.Model.extend({
		localStorageKey: 'USER',
		
		persist: function()
		{
			return window.localStorage.setItem(this.localStorageKey, JSON.stringify(this.attributes));
		},

		load: function()
		{
			var persistedData = window.localStorage.getItem(this.localStorageKey);
			if (persistedData === null)
			{
				return null;
			}
			else
			{
				console.debug('Loaded model from localStorage key '+ this.id);
				var data = JSON.parse(persistedData)
				this.set(data);
				return true; 
			}
		},

		updatePoints: function()
		{
			var attendanceUri = Settings.resourceUriFor('attendance/' + this.get('attendance_pk'));
			var self = this;
			if (attendanceUri) $.get(attendanceUri, null, function(data)
			{
				data = JSON.parse(data);
				self.set('points', data.points);
				self.persist();
			});
		},
	});
});