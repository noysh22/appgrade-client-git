define(['app/settings', 'underscore', 'backbone', 'app/helpers/path', 'app/helpers/string'], function(Settings, _, Backbone, Path, StringHelper)
{
	// var LOCAL_STORAGE_KEY_PREFIX = 'MODEL_';

	function appendSlash(str)
	{
		// console.debug('appendSlash: "{0}" => "{1}"'.format(str, str + ((str.length > 0 && str.charAt(str.length - 1) === '/') ? '' : '/')));
		return str + ((str.length > 0 && str.charAt(str.length - 1) === '/') ? '' : '/');
	}

	var LOCAL_STORAGE_KEY_PREFIX = 'MODEL_';

	var model = Backbone.Model.extend({
		idAttribute: 'resource_uri',
		urlRoot: Settings.API_ENDPOINT,

		url: function()
		{
			// Use `resource_uri` if possible
			var root = _.result(this, 'urlRoot');
			var url = this.has('resource_uri') ? Path.join(root, this.get('resource_uri')) : null;

			// If there's no idAttribute, use the `urlRoot`. Fallback to try to have the collection construct a url.
			// Explicitly add the `id` attribute if the model has one.
			if (url === null)
			{
				url = url || this.collection && _.result(this.collection, 'url');

				if (url && this.has('id'))
					url = appendSlash(url) + this.get('id');
			}

			url = url && appendSlash(url);
			return url || null;
		},

		/**
		 * Extracts model data from the response: either the first entry in `response.objects` if it exists and is an array,
		 * or the entire response otherwise.
		 * 
		 * @return {object} Model data object from response
		 */
		parse: function(res, options)
		{
			return res && res.objects && (_.isArray(res.objects) ? res.objects[0] : res.objects) || res;
		},

		/**
		 * Default persistence mechanism. Uses localStorage to save the model's JSON representation.
		 * 
		 * @return {string} localStorage key
		 */
		persist: function()
		{
			if (!this.id) return null;
			return window.localStorage.setItem(LOCAL_STORAGE_KEY_PREFIX + this.id, JSON.stringify(this));
		},

		/**
		 * Retreives the model's attributes from the server or parses them from localStorage.
		 *
		 * @param {function} callback Function to call after loading the model
		 */
		load: function(callback)
		{
			if (!this.id) return null;

			var persistedData = window.localStorage.getItem(LOCAL_STORAGE_KEY_PREFIX + this.id);
			if (persistedData === null)
			{
				this.fetch({success: callback});
			}
			else
			{
				console.debug('Loaded model from localStorage key '+ this.id);
				var data = JSON.parse(persistedData)
				this.set(data);
				if(callback) callback(this, data, {});
			}
		},
		
		_getId: function()
		{
			if (this.has('id')) return this.get('id');
			return _.chain(this.get('resource_uri').split('/')).compact().last().value();
		}
	});
	
	
	var collection = Backbone.Collection.extend({
		urlRoot: Settings.API_ENDPOINT,

		/**
		 * Extracts collection data from the response: either `response.objects` if it exists and is an array.
		 * If present, `response.meta` object is assigned to the collection's `meta` instance variable.
		 * 
		 * @return {object} Model data object from response
		 */
		parse: function(response)
		{
			if (response && response.meta)
				this.meta = response.meta;
			return response && response.objects || response;
		},

		url: function(models)
		{
			// Use `urlRoot` if possible
			var url = _.result(this, 'urlRoot');

			// If the collection doesn't specify a URL root, try to obtain one from a model in the collection
			if (!url)
			{
				var model = models && models.length && models[0];
				url = model && _.result(model, 'url');
			}
			url = url && appendSlash(url);

			// Build a URL to retrieve a set of models. This assumes the last part of each model's idAttribute
			// (set to 'resource_uri') contains the model's ID.
			if (models && models.length)
			{
				var ids = _.map(models, function(model) { return model._getId(); });
				url += 'set/' + ids.join(';') + '/';
			}

			return url || null;
		},

		/**
		 * Default persistence mechanism. Uses localStorage to save the collection's JSON representation.
		 * 
		 * @return {string} localStorage key
		 */
		persist: function()
		{
			if (!this.urlRoot) return null;
			return window.localStorage.setItem(LOCAL_STORAGE_KEY_PREFIX + this.urlRoot, JSON.stringify(this));
		},

		/**
		 * Retreives the models' attributes from the server or parses them from localStorage.
		 *
		 * @param {function} callback Function to call after loading the model
		 */
		load: function(callback)
		{
			if (!this.urlRoot) return null;

			var persistedData = window.localStorage.getItem(LOCAL_STORAGE_KEY_PREFIX + this.urlRoot);
			if (persistedData === null)
			{
				this.fetch({success: callback});
			}
			else
			{
				console.debug('Loaded collection from localStorage key '+ this.urlRoot);
				var data = JSON.parse(persistedData)
				this.set(data);
				callback(this, data, {});
			}
		}
	});
	
	
	var eventChildrenCollection = collection.extend({
		/**
		 * Appends the event filter to the endpoint URL.
		 * Note: subclasses must append other URL parameters with an ampersand, and not a question mark.
		 * 
		 * @return {string} Collection endpoint URL
		 */
		url: function(models)
		{
			var base = collection.prototype.url.apply(this, models);
			return '{0}?event_pk={1}'.format(base, window.sessionStorage.getItem('EVENT_ID'));
		}
	});


	return {Model: model, Collection: collection, EventChildrenCollection: eventChildrenCollection};
});