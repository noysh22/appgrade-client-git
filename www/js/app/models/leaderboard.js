define(['app/settings', 'backbone', './base'], function(Settings, Backbone, Base)
{
	'use strict';


	var leaderboardCollection = Base.EventChildrenCollection.extend({
		urlRoot: Settings.resourceUriFor('leaderboard'),
	});
	
	return leaderboardCollection;
});