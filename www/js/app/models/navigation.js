define(['app/settings', 'backbone', './base'], function(Settings, Backbone, Base)
{
	"use strict";

	var linkModel = Base.Model.extend();
	
	var linkCollection = Base.EventChildrenCollection.extend({
		urlRoot: Settings.resourceUriFor('navigation'),
		model: linkModel
	});

	return {LinkModel: linkModel, LinkCollection: linkCollection};
});