define(['app/settings', './base', 'app/helpers/path'], function(Settings, Base, PathHelper)
{
	"use strict";
	
	return Base.Model.extend({
		parse: function(res, options) 
		{
			// Prepend the server path to the photo URL.
			// TODO: Change the server to return a full URL.
			_.each(res.media.slideshow, function(slideshow, index)
			{
				res.media.slideshow[index] = PathHelper.join(Settings.SERVER_ROOT, slideshow);
			});

			return Backbone.Model.prototype.parse.call(this, res, options);
		},

		update: function ()
		{
			this.id = window.localStorage.getItem(Settings.EVENT_ID_KEY); // id of the event
			this.url = Settings.SERVER_ROOT + this.id;
			
			this.load();	

			var self = this;
			this.fetch({
				success: function (model, response)
				{
					model.persist();	
				}
			});	
		},
	});
});