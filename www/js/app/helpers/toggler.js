define(['zepto'], function($)
{
	'use strict';
	
	/**
	* Toggles a button disabled attribute,
	* @param {String} text, text to change in the button new state.
	*/
	$.extend($.fn, {
		toggleButton: function(text)
		{
			var button = _.first(this);
			button.disabled = !button.disabled;
			// el.disabled = !el.disabled;

			if(text) button.innerText = text;
		}
	});

	return $;
});