define(['zepto'], function($)
{
	'use strict';
	
	// $.fn.serializeForm = function()
	// {
	// 	var result = {};
	// 	this.serializeArray().forEach(function(el)
	// 	{
	// 		result[el.name] = el.value;
	// 	});
	// 	return result;
	// };

	/**
	 * Serializes the inputs of form elements to a plain hash object.
	 * @return {object} Hash in the form of {name1: value1, name2: value2}
	 */
	$.extend($.fn, {
		serializeForm: function()
		{
			var result = {};
			this.serializeArray().forEach(function(el)
			{
				result[el.name] = el.value;
			});
			return result;
		}
	});

	return $;
});