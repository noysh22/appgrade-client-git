define(['require', 'app/settings', 'app/helpers/path', 'handlebars'],
function(require, Settings, Path, Handlebars)
{
	'use strict';

	var messages = {};
	
	require([Path.join(Settings.LOCALE_PATH, Settings.UI_LANGUAGE)], function(locale)
	{
		messages = locale;
	});
	
	function translate(msg)
	{
		if (messages.hasOwnProperty(msg))
			return messages[msg];
		return msg;
	};


	function LazyString(fn, arg)
	{
		this.fn = fn;
		this.arg = arg;
	}

	LazyString.prototype.toString = function()
	{
		return this.fn(this.arg);
	};

	function translate_lazy(msg)
	{
		return new LazyString(translate, msg);
	}


	Handlebars.registerHelper('t', function(msg)
	{
		return new Handlebars.SafeString(translate(msg));
	});


	return {
		translate:		translate,
		t:				translate,
		translate_lazy:	translate_lazy,
		l:				translate_lazy
	};
});