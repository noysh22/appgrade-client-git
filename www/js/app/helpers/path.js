// Adapted from http://docs.closure-library.googlecode.com/git/closure_goog_string_path.js.source.html
define(function(require)
{
	'use strict';

	require('app/helpers/string');

	// Copyright 2010 The Closure Library Authors. All Rights Reserved.
	//
	// Licensed under the Apache License, Version 2.0 (the "License");
	// you may not use this file except in compliance with the License.
	// You may obtain a copy of the License at
	//
	//      http://www.apache.org/licenses/LICENSE-2.0
	//
	// Unless required by applicable law or agreed to in writing, software
	// distributed under the License is distributed on an "AS-IS" BASIS,
	// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	// See the License for the specific language governing permissions and
	// limitations under the License.
	return {
		/**
		* Joins one or more path components (e.g. 'foo/' and 'bar' make 'foo/bar').
		* Unlike Python's version, an absolute component will discard all previous components *except the first*.
		* See http://docs.python.org/library/os.path.html#os.path.join
		* @param {...string} One of more path components.
		* @return {string} The path components joined.
		*/
		join: function()
		{
			var root = arguments[0];
			var path = root;

			for (var i = 1; i < arguments.length; i++)
			{
				var arg = arguments[i];
				
				if (arg.startsWith('/'))
				{
					if (root.endsWith('/'))
						arg = arg.substr(1);
					path = root + arg;
				}
				else if (path == '' || arg.endsWith('/'))
					path += arg;
				else
					path += '/' + arg;
			}

			return path;
		}
	};
});