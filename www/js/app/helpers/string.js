define(function(require)
{
	"use strict";

	/**
	 * Formats a string with parameters.
	 * Example: "{0} is dead, but {1} is alive! {0} {2}".format("ASP", "ASP.NET")
	 * 			-> "ASP is dead, but ASP.NET is alive! ASP {2}"
	 * @return String
	 */
	String.prototype.format = function()
	{
		var args = arguments;
		var sprintfRegex = /\{(\d+)\}/g;

		var sprintf = function(match, number)
		{
			return number in args ? args[number] : match;
		};

		return this.replace(sprintfRegex, sprintf);
	};


	// Adapted from http://docs.closure-library.googlecode.com/git/closure_goog_string_string.js.source.html
	
	/**
	* Fast prefix-checker.
	* @param {string} str The string to check.
	* @param {string} prefix A string to look for at the start of {@code str}.
	* @return {boolean} True if {@code str} begins with {@code prefix}.
	*/
	String.prototype.startsWith = function(prefix)
	{
		return this.lastIndexOf(prefix, 0) == 0;
	};

	/**
	* Fast suffix-checker.
	* @param {string} str The string to check.
	* @param {string} suffix A string to look for at the end of {@code str}.
	* @return {boolean} True if {@code str} ends with {@code suffix}.
	*/
	String.prototype.endsWith = function(suffix)
	{
		var l = this.length - suffix.length;
		return l >= 0 && this.indexOf(suffix, l) == l;
	};
});