define([
	'app/settings', 'app/helpers/i18n', 'zepto', 'backbone', 'handlebars',
	'./base', '../models/leaderboard',
	'text!templates/leaderboard/leaderboard.html'
],
 function(Settings, i18n, $, Backbone, Handlebars, BaseView, LeaderboardCollection, LEADERBOARD_TEMPLATE_SOURCE)
{
	'use strict';

	Handlebars.registerHelper('inc', function(number)
	{
		return number + 1;
	});
	
	return BaseView.extend({
		pageTitle: i18n.l('Leaderboard'),

		templateSource: LEADERBOARD_TEMPLATE_SOURCE,

		initialize: function(event_pk)
		{
			this.collection = new LeaderboardCollection();
			// this.collection.comparator = 'points';

			var self = this;
			this.collection.fetch({
				success: function ()
				{					
					self.trigger('ready');
				}
			});
		},

		isReady: function()
		{
			return this.collection.length > 0;
		},

		render: function()
		{
			this.$el.html(this.renderTemplate({users: this.collection.models }));

			return this;
		}
	});
});