define([
	'app/settings', 'app/helpers/i18n', 'zepto', 'app/helpers/form', 'app/helpers/string', 'backbone',
	'./base', 'app/models/user', 'app/views/login','text!templates/register.html'
],
function(
	Settings, i18n, $, FormHelper, StringHelper, Backbone,
	BaseView, UserModel, LoginView, REGISTER_TEMPLATE_SOURCE
)
{
	'user strict';

	return BaseView.extend({
		allowSidebar: false,

		pageTitle: i18n.l('Register'),

		templateSource: REGISTER_TEMPLATE_SOURCE,

		events: {
			'submit form' : 'register',
			'blur input' : function(event) { 
				event.preventDefault();
				$(event.currentTarget).addClass('interacted');
			}
		},

		render: function ()
		{
			console.debug('Rendering Register View');
			window.AppVent.trigger('sidebar:disable');
			this.$el.html(this.renderTemplate());
			return this;
		},

		register: function (event) 
		{
			event.preventDefault();
			var formValues = this.$('form#register').serializeForm();
			var splitName = formValues.fullname.trim().split(' ');

			formValues.first_name = splitName[0];
			formValues.last_name = splitName[1];

			var self = this;

			$.ajax({
				url: Settings.resourceUriFor('user/' + formValues.username + '/'),
				type: 'PUT',
				data: JSON.stringify(formValues),
				dataType: 'json',
				success: function (data)
				{
					if (data.error) alert(data.error.text);
					else if (data.errors) alert(data.errors);
					else
					{
					    console.log('User created successfuly');
					   					    
					    var user = new UserModel(_.extend(data['user'], { password: formValues.password }));
					    user.persist();
					    window.EventUser = user;
					    window.localStorage.setItem(Settings.EVENT_ID_KEY, data.event_resource_uri);
						self.finalize();			
					}
				},
				error: function(xhr, type) 
				{
					alert(xhr.message);
				}				 
			});
		},

		finalize: function ()
		{
		    new LoginView(); // init Login view to go to the main page directly.
			this.destroy();
		},
	});
});