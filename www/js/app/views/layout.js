define(function(require)
{
	'use strict';

	var $ = require('zepto'), _ = require('underscore'), Backbone = require('backbone'),
		Settings = require('app/settings'), Spinner = require('vendor/spin.min'),
		BaseView = require('./base'), SidebarView = require('./sidebar'), UserModel = require('app/models/user');
	
	var SIDEBAR_CLASS = 'deploy-sidebar';

	return BaseView.extend({
		el: 'body',

		ui: {
			'mainContainer': '#page-container',
			'pageTitle': 'header h1',
			'activityIndicatorContainer': 'div#content-panels',
			'sidebarDeployBtn': 'button.ion-navicon',
			'banner': 'article#banner',
			'footerLeft': 'footer.page-footer #leftImg',
			'footerRight': 'footer.page-footer #rightImg'
		},

		events: {
			'click button.ion-navicon':	'deploySidebar',
			'click a.close-nav':			'deploySidebar',
		},

		defaultBanner: 'img/logoimg.png',

		initialize: function(options)
		{
			this.bindUI();
			
			this.sidebarEnabled = true;

			var self = this;
			window.AppVent.once('authenticated', function()
			{
				self.sidebarView = new SidebarView({contentEl: self.ui.mainContainer.parent()[0]});
				self.render();
			});

			this.mainView = null;
			this.spinner = new Spinner(Settings.SPIN_OPTIONS);

			// Create user model in case in wasn't created in the login view
			if(window.localStorage.getItem('USER') && !window.EventUser)
			{
				window.EventUser = new UserModel();
				window.EventUser.load();
			}

			var sidebarDeployBtn = this.ui.sidebarDeployBtn;
			window.AppVent.on('sidebar:disable', _.bind(this.disableSidebar, this));
			window.AppVent.once('event:sync', this.loadBanner, this);
		},

		loadBanner: function (eventModel)
		{
			var eventMedia = eventModel.get('media');

			if(!eventMedia || !eventMedia['footer-right'])
			{
				return;
			}
			var $img = $('<img>');

			$img.attr('class', 'banner');

			$img.attr('src', Settings.SERVER_ROOT + eventMedia['footer-right']);
			this.ui.banner.toggleClass('hide');
			this.ui.banner.html($img);
		},

		deploySidebar: function(event) 
		{
			if (this.sidebarEnabled)
			{
				this.sidebarView.open(event);
			}
		},

		disableSidebar: function()
		{
			this.sidebarEnabled = false;
			$('button.ion-navicon').css('display', 'none');
			this.ui.sidebarDeployBtn.removeClass(SIDEBAR_CLASS);
		},

		setMainView: function(view)
		{
			// No point in displaying a view that has been destroyed...
			if (view.isDestroyed()) return;

			if (!view.isReady())
			{
				console.log('Deferring view rendering', view);

				this.showActivityIndicator();

				view.once('ready', function()
				{
					console.debug('Recieved "ready" event from deferred view');
					this.hideActivityIndicator();
					this._renderMain(view);
				}, this);

				view.once('destroy', function()
				{
					console.log('Recieved "destroy" event from deferred view');
					this.hideActivityIndicator();
				}, this);
			}
			else this._renderMain(view);
		},

		_renderMain: function(view)
		{
			this.resetMainView();
			this.mainView = view;

			if (this.hasOwnProperty('sidebarView'))
			{
				this.sidebarEnabled = true;
				this.sidebarView.enable();
				this.ui.sidebarDeployBtn.addClass(SIDEBAR_CLASS);
			}
			
			this.ui.pageTitle.text(_.result(view, 'pageTitle'));
			this.ui.mainContainer.append(view.render().el);
		},

		showActivityIndicator: function()
		{
			console.debug('Showing activityIndicator in element', this.ui.activityIndicatorContainer[0]);
			this.spinner.spin(this.ui.activityIndicatorContainer[0]);
		},

		hideActivityIndicator: function()
		{
			console.debug('Hiding activityIndicator');
			this.spinner.stop();
		},

		resetMainView: function()
		{
			console.log('Resetting main view container');

			if (this.mainView !== null)
			{
				if (!this.mainView.isDestroyed()) // Rare are the views that destroy themselves...
					this.mainView.destroy();
				this.mainView = null;
			}
			this.ui.mainContainer.html('');
		}
	});
});