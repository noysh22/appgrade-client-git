define([
	'app/settings', 'app/helpers/i18n', 'zepto', 'app/helpers/form', 'app/helpers/string', 'app/helpers/toggler', 'backbone',
	'./base', 'app/models/user', 'text!templates/login.html', 'handlebars'
],
function(
	Settings, i18n, $, _FormHelper, _StringHelper, _ToggleHelper, Backbone,
	BaseView, UserModel, LOGIN_TEMPLATE_SOURCE, Handlebars
)
{
	'use strict';

	return BaseView.extend({
		redirectTo: '',

		allowsSidebar: false,

		pageTitle: i18n.l('Login'), 

		initialize: function(options)
		{
			if (options && options.hasOwnProperty('redirectTo')) this.redirectTo = options.redirectTo;
			
			console.log('Checking authentication');
			var user = new UserModel();
			if (user.load() !== null && window.localStorage.getItem(Settings.EVENT_ID_KEY) !== null)
			{
				console.log('Authentication localStorage keys present for user and event');
				window.EventUser = user;
				this.finalize();
			}
		},

		templateSource: LOGIN_TEMPLATE_SOURCE,

		events: {
			'submit form#authentication': 'authenticate',
			'blur input' : function(event) { 
				event.preventDefault();
				$(event.currentTarget).addClass('interacted');
			}
		},

		render: function()
		{
			console.debug('Rendering LoginView');
			window.AppVent.trigger('sidebar:disable');
			this.$el.html(this.renderTemplate());

			return this;
		},

		authenticate: function(event)
		{
			$("#locked-msg").remove();

			event.preventDefault(); // Don't let the form submit

			$('button[type=submit]').toggleButton(i18n.t('Logging In'));
			var formValues = this.$('form#authentication').serializeForm();
			
			var self = this;
			$.ajax({
				type: 'POST',
				url: Settings.resourceUriFor('user/login/'),
				data: JSON.stringify(formValues),
				contentType: 'application/json',
				success: function (data) {
					var user = new UserModel(_.extend(data['user'], { password: formValues.password }));
					user.persist();
					window.EventUser = user;
					window.localStorage.setItem(Settings.EVENT_ID_KEY, data.event_resource_uri);
					self.finalize();			
				},
				error: function (xhr, status) { // in case of error in login
					$('button[type=submit]').toggleButton(i18n.t('Log In'));
					self.handleLoginErrors(xhr);
				},
			});
		},

		handleLoginErrors: function(xhr)
		{
			var error = JSON.parse(xhr.response);

			if (xhr.status === 401)
			{
				if (error.reason === "invalid_event")
				{
					window.AppAlert(i18n.t('Event Id is not correct'), 
						null, i18n.t('Failed to login'), i18n.t('OK'));	
				}
				else if (error.reason === "incorrect")
				{
					window.AppAlert(i18n.t('Incorrect username or password'), 
						null, i18n.t('Failed to login'), i18n.t('OK'));
				}
			}
			else if (xhr.status === 423 && error.reason === "inactive_event")
			{
				var template = Handlebars.compile($("#event-closed").html());
				$(template()).insertBefore("section.list");
			}
			else if (xhr.status >= 500)
			{
				window.AppAlert(i18n.t('Cannot login, error occured during authentication'), 
						null, i18n.t('Failed to login'), i18n.t('OK'));
			}
		},

		persistSessionHeader: function(user)
		{
			var creds = '{0}:{1}'.format(user.username, user.password);
			console.log('Setting AJAX authentication credentials '+ creds);
			$.ajaxSettings.headers = {'Authorization': 'Basic ' + btoa(creds)};
		},

		setEventSession: function()
		{
			var eventUri = window.localStorage.getItem(Settings.EVENT_ID_KEY);
			if (eventUri)
			{
				var eventId = eventUri.split('/')[Settings.EVENT_URI_ID_INDEX];
				window.sessionStorage.setItem(Settings.EVENT_ID_KEY, eventId);
			}
		},

		finalize: function()
		{
			console.log('finalized login');
			this.persistSessionHeader(window.EventUser.attributes);
			this.setEventSession();
			window.AppVent.trigger('authenticated');
			window.AppRouter.goTo(this.redirectTo);
			this.destroy();
		}
	});
});
