define(['app/settings', 'zepto', 'vendor/fx.zepto', 'vendor/slide.zepto.min', 'handlebars',
		'./base', 'app/models/base', 'app/models/event', 'app/models/navigation',
		'text!templates/home.html', 'text!templates/home_nav.html'],
function(
	Settings, $, $_Fx, $_Slide, Handlebars, BaseView, BaseModel, EventModel,
	Navigation, HOME_TEMPLATE_SOURCE, NAVIGATION_TEMPLATE_SOURCE
)
{
	'use strict';

	Handlebars.registerHelper('slice', function(context, options)
	{
		var offset = options.hash.offset || 0, limit = options.hash.limit || context.length,
			i = offset, j = ((limit + offset) < context.length) ? (limit + offset) : context.length;
		var result = '';

		for (i, j; i < j; i++)
			result += options.fn(_.extend({'itemIndex': i}, context[i]));
		return result;
	});

	var HERO_ACTIVE_CLASS = 'hero-active';
	var HERO_INEACTIVE_CLASS = 'hero-disabled';
	
	var homeViewModel = BaseModel.Model.extend({
		initialize: function(options) 
		{
			var self = this;
			window.EventUser.on('change:points', function (newUser) {
				self.set('points', newUser.get('points'));
			});
		}
	});

	return BaseView.extend({
		initialize: function(options)
		{
			var self = this;

			this.model = new EventModel({'resource_uri': options.eventId});
			this.model.on('sync', this.model.persist, this.model);

			this.model.load(function(model)
			{
			    console.debug('Model loaded: ' + model.id);
                // on.sync did not brought all the attributes of the event so trigger the event here.
			    window.AppVent.trigger('event:sync', model);

			    if (model.get('media').logo && 
			    	-1 == model.get('media').logo[0].indexOf(Settings.SERVER_ROOT))
			    	model.get('media').logo[0] = Settings.SERVER_ROOT + model.get('media').logo[0];

			    if (model.get('media').sideBg &&
			    	-1 == model.get('media').sideBg[0].indexOf(Settings.SERVER_ROOT))
			    {
			    	model.get('media').sideBg[0] = Settings.SERVER_ROOT + model.get('media').sideBg[0];
			    }

				self.trigger('ready');
			});

			this.navigationTemplate = Handlebars.compile(NAVIGATION_TEMPLATE_SOURCE);
			this.linkCollection = new Navigation.LinkCollection();
			this.linkCollection.on('sync', this.linkCollection.persist, this.linkCollection);

			this.on('render', function()
			{
				self.linkCollection.load(function(models)
				{
					console.log('loaded links', self.linkCollection.toJSON());
					self.ui.gameMenu.html(self.navigationTemplate({links: self.linkCollection.toJSON()}));
					
					self.bindUI();
				});
			});
		},

		templateSource: HOME_TEMPLATE_SOURCE,

		ui: {
			'gameMenu'	: 'nav#game-menu',
			'hero'		: 'nav#game-menu div#hero',
		},

		customStyleSelector: 'style#custom-style',

		sidebarImage: '#sidebar-image',

		events: {
			'click nav#game-menu article': 'gameSelected',
		},
		
		bindings: {
			'#attendee-score' : 'points', // Score: 0
			'#attendee-rank' : 'attendance_rank',
			'#event-score' : 'aggregate_points', // Total Event Score
			'#event-image': { attributes: [ { name:'src', observe: 'event_logo'} ] } // event logo
		},

		bindSidebarImage: function(imageUrl)
		{
			var cssString = 'url(' + imageUrl + ') no-repeat';
			$(this.sidebarImage).css('background', cssString);
			$(this.sidebarImage).css('background-size', 'cover');
		},

		pageTitle: function()
		{
			return this.model.get('title');
		},

		isReady: function()
		{
			return this.model.has('title');
		},

		bindData: function(event)
		{
			var curEvent = this.__getEvent();
			var viewModelAttributes = {};

			if (window.EventUser)
			{
				_.extend(viewModelAttributes, window.EventUser.attributes);
				
				if (this.model.get('aggregate_points')) 
				{
					_.extend(viewModelAttributes, { aggregate_points: this.model.get('aggregate_points') });
				}
				else if (curEvent && curEvent.aggregate_points) 
				{
					_.extend(viewModelAttributes, { aggregate_points: curEvent.aggregate_points });
				}

				if (this.model.get('media').logo) 
				{
					viewModelAttributes.event_logo = this.model.get('media').logo[0];
				};

				this.model = new homeViewModel(viewModelAttributes);
				this.stickit();
			}
		},

		__getEvent: function ()
		{
			var eventId = window.localStorage.getItem(Settings.EVENT_ID_KEY);
			return JSON.parse(window.localStorage.getItem('MODEL_' + eventId));
		},

		render: function()
		{
			this.$el.html(this.renderTemplate({event: this.model.toJSON()}));
			this.bindUI();
			//populate the costum stylesheet for the app.
			$(this.customStyleSelector).html(this.model.get('stylesheet'));
			this.bindSidebarImage(this.model.get('media').sideBg[0]); // bind sidebar image to sidebar

			this.bindData();
			this.trigger('render');
			return this;
		},

		/**** Hero functions ****/

		_isHeroOpen: false,

		setHeroLinkData: function(linkIndex)
		{
			var selectedLink = this.linkCollection.at(parseInt(linkIndex));
			var iconClass = 'flaticon-' + selectedLink.get('classification') + ' nav-icon';

			this.ui.hero.find('p').text(selectedLink.get('description'));
			this.ui.hero.find('a').attr('href', '#' + selectedLink.get('route'));
			this.ui.hero.find('#icon').removeClass().addClass(iconClass);
		},

		toggleHeroListener: function(isOn)
		{
			if(isOn)
			{
				$(document).on('click', function(e) {
				    if ((e.target.className !== 'menu-content playgo-menu-items-bg' 
				    	&& e.target.parentElement.className !== 'menu-content playgo-menu-items-bg')) 
				    {
				      this.closeHero()
				    }
				  }.bind(this));
			}
			else
			{
				$(document).off('click');
			}
		},

		/*
		 *	TODO: dry the following two functions
		 */

		closeHero: function()
		{
			if(this._isHeroOpen)
			{		
				this.ui.hero.removeClass(HERO_ACTIVE_CLASS);
				this.ui.hero.addClass(HERO_INEACTIVE_CLASS);
				this._isHeroOpen = false;

				this.toggleHeroListener(false);
			}
		},

		openHero: function()
		{
			this.ui.hero.addClass(HERO_ACTIVE_CLASS);
			this.ui.hero.removeClass(HERO_INEACTIVE_CLASS);
			this._isHeroOpen = true;

			this.toggleHeroListener(true);
		},

		gameSelected: function(event) 
		{
			event.preventDefault();

			// var hero = $(this.ui.hero);
			var gameArticle = $(event.target).closest('article');

			this.openHero();

			this.setHeroLinkData(gameArticle.data('index'));
			var hero = this.ui.hero;
			
			setTimeout(function() {
				if (hero.scrollIntoViewIfNeeded) 
					hero.scrollIntoViewIfNeeded(); 
			}, 170);
		}
	});
});