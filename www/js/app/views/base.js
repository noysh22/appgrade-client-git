define(['app/settings', 'app/helpers/i18n', 'underscore', 'backbone', 'backbone.stickit.min', 'handlebars'], 
	function(Settings, i18n, _, Backbone, Stickit, Handlebars)
{
	return Backbone.View.extend({
		_destroyed: false,

		allowsSidebar: true,

		/**
		 * Closes the view and clean its UI bindings and events.
		 */
		destroy: function()
		{
			this.trigger('destroy');
			this.unbindUI();
			this.remove();
			this.unbind();
			this._destroyed = true;
		},

		/**
		 * Determines if the view has been destroyed.
		 */
		isDestroyed: function()
		{
			return this._destroyed;
		},

		/**
		 * Decides if the view is ready for rendering. Subclasses may override with their own logic; for example, asynchronous views may
		 * return false for as long as the data is being loaded.
		 * 
		 * @return {boolean} True if the view can be rendered, false otherwise.
		 */
		isReady: function()
		{
			return true;
		},

		/**
		 * Binds the elements specified in the `ui` hash in the view with the respective Zepto/jQuery selectors.
		 * Copied from Marionette.js
		 */
		bindUI: function(){
			if (!this.ui) return;

			// store the ui hash in _uiBindings so they can be reset later
			// and so re-rendering the view will be able to find the bindings
			if (!this._uiBindings)
				this._uiBindings = this.ui;

			// get the bindings result, as a function or otherwise
			var bindings = _.result(this, '_uiBindings');

			// empty the ui so we don't have anything to start with
			this.ui = {};

			// bind each of the selectors
			for (var key in bindings)
			{
				var selector = bindings[key];
				this.ui[key] = this.$(selector);
			}
		},

		/**
		 * Unbinds the elements specified in the `ui` hash.
		 * Copied from Marionette.js
		 */
		unbindUI: function()
		{
			if (!this.ui || !this._uiBindings) return;

			// delete all existing UI bindings
			_.each(this.ui, function($el, name)
			{
				delete this.ui[name];
			}, this);

			// reset the `ui` element to the original binding configuration
			this.ui = this._uiBindings;
			delete this._uiBindings;
		},

		_templateCache: null,

		/**
		 * Renders the template with the given context. Compiles the template if needed.
		 * @param  {object} ctx Template rendering context
		 * @return {string}     Rendered template
		 */
		renderTemplate: function(ctx)
		{
			if (this._templateCache === null)
				this._templateCache = Handlebars.compile(this.templateSource);
			return this._templateCache(ctx);
		},

		injectNestedView: function (viewToInject, selector)
		{
			if (viewToInject)
			{
				var nestedView = viewToInject.render().$el;
				console.debug('Injecting', viewToInject, 'with content', nestedView);
				$(selector).html(nestedView);
			}
		},
	});
});