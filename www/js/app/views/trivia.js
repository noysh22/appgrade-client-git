define(function(require)
{
	'use strict';

	var Settings = require('app/settings'), i18n = require('app/helpers/i18n');
	var _ = require('underscore'), $ = require('zepto'), Backbone = require('backbone');
	var BaseView = require('./base'), TriviaModels = require('app/models/trivia');

	var GameOverView = BaseView.extend({
		templateSource: require('text!templates/trivia/game_over.html'),
		render: function() { this.$el.html(this.renderTemplate()); return this; }
	});

	var QuestionView = BaseView.extend({
		templateSource: require('text!templates/trivia/question.html'),

		ui: {
			'img' : '#question-image',
			'skipBtn' : '#skip-btn',
		},

		bindings: {
			'#user-score': 'points',
		},

		initialize: function(options)
		{
			console.log('Initializing QuestionView with model:', options.model);
		},

		events: {
			'click #skip-btn': function(event) { 
				event.preventDefault();
				this.trigger('trivia:skip'); 
			},
			'click .answer' : function(event) { 
				event.preventDefault();
				// TODO: support multiple answers in a submission
				var questionId = event.currentTarget.id.split('_');
				this.trigger('trivia:answer', {
					data: {
						question_pk: this.model.get('id'),
						answer_option_choices: [ questionId[1] ]
					},
					model: this.model,
				}); 
			} 
		},

		isReady: function()
		{
			return this.model.has('title');
		},

		toggleImage: function()
		{
			// Hide image tag when there is no image in the question
			if (!this.model.has('image')) this.$(this.ui.img).hide();
			else 
			{
				this.ui.img.attr('src', Settings.SERVER_ROOT + this.model.get('image'));
				this.ui.img.show('fast');
			}
		},

		render: function()
		{
			console.log('Rendering question', this.model);
			var modelData = null;
			if(this.model) modelData = this.model.toJSON();

			this.$el.html(this.renderTemplate({question: modelData }));

			if (this.model) 
			{
				this.bindUI();
				this.toggleImage();

				if (window.EventUser !== null && window.EventUser !== undefined) 
				{
					this.stickit(window.EventUser);
				}		
			};
				
			return this;
		},
	});

	var QuestionGroupView = BaseView.extend({
		pageTitle: i18n.l('Trivia'),

		ui: {
			'questionContainer': '#question-container',
		},

		initialize: function(options)
		{
			console.log('Initializing QuestionGroupView with options:', options);
			this.collection = new TriviaModels.QuestionCollection(null, {groupId: options.questionGroupId});
			this.questionView = null;
			this.skipQuestion = _.bind(this.skipQuestion, this);
			this.answerQuestion = _.bind(this.answerQuestion, this);

			this.gameOverView = new GameOverView();

			var self = this;
			this.collection.fetch({	reset: true, });

			// after the collection is fetched its being reset, and them a filter can be apllied
			this.collection.once('reset', function (collection) 
			{
				// self.collection.models = self.collection.where( { is_answered: false } );
				self.collection.set(collection.where( { is_answered: false } ));				
					
				if(self.collection.models.length > 0)
				{
					self.updateQuestion();
				}

				self.trigger('ready');
			});
		},

		rendered: false,

		templateSource: require('text!templates/trivia/question_session.html'),

		isReady: function()
		{
			console.log('Delegating isReady() to child question view', this.questionView);
			return this.questionView && this.questionView.isReady();
		},

		updateQuestion: function()
		{
			console.debug('Updating question view; Previous:', this.questionView);
			if (this.questionView && this.questionView.isDestroyed() === false)
			{
				console.debug('Destroying previous question view');
				this.questionView.off('trivia:skip', this.skipQuestion); // TODO: make sure this is really needed
				this.questionView.off('trivia:answer', this.answerQuestion);	
				this.questionView.destroy();
			}
			
			//TODO: Check here if the index of the question is the last index of the collection.
			var qModel = this.collection.at(0);
			console.debug('Initializing QuestionView with model', qModel);
			this.questionView = new QuestionView({model: qModel});
			this.questionView.on('trivia:skip', this.skipQuestion);
			this.questionView.on('trivia:answer', this.answerQuestion);
			this.questionView.render();

			this.injectNestedView(this.questionView, this.ui.questionContainer);

			console.debug('Element after rendering questionView:', this.questionView.el);
		},

		render: function()
		{
			if (!this.rendered) {
				console.debug('Rendering QuestionGroupView once');
				this.$el.html(this.renderTemplate());
				this.bindUI();
				this.rendered = true;
			}

			if (this.collection.models.length > 0)
			{
				this.injectNestedView(this.questionView, this.ui.questionContainer);

				if(this.collection.models.length == 1) this.questionView.ui.skipBtn.hide();
			} 
			else 
			{
				this.injectNestedView(this.gameOverView, this.ui.questionContainer);
			}
			
			return this;
		},

		nextQuestion: function()
		{
			// this.collection.remove(this.collection.at(this.questionIndex));
			// ++this.questionIndex;
			if(this.collection.models.length > 1)
			{
				var questionToSkip = this.collection.shift();
				this.collection.push(questionToSkip);	
			}
			else
			{
				// TODO: change this to a more user friendly alert - notification inside the challenge view.				
				window.AppAlert(i18n.t('Sorry, this is the last question in the game.'), 
						null, null, i18n.t('OK'));
			}
		},

		skipQuestion: function(options)
		{
			//TODO: make sure its not possible to skip the last question
			this.nextQuestion();
			this.updateQuestion();					
		},

		answerQuestion: function(options)
		{
			var answerUrl = this.collection.urlRoot + '/' + options.data.question_pk + '/answer/';
			var groupId = this.collection.groupId;
			console.log('answering question %s', answerUrl);
			options.data.attendance_pk = window.EventUser.get('attendance_pk');
			var self = this;

			$.ajax({
				url: answerUrl,
				type: 'POST',
				contentType: 'application/json',
				data: JSON.stringify(options.data),
				success: function(data) {
					// TODO: navigate to explenaton view with the appropiate params
					// consider using and event and pass it the appropeate params
					var questionExplanation = { explanation: options.model.get('explanation') };
					if(data.is_correct)
					{
						questionExplanation.resultClass = 'correct';
						questionExplanation.result = i18n.t('Correct');
						
						window.EventUser.updatePoints();
					}
					else
					{
						questionExplanation.resultClass = 'wrong';
						questionExplanation.result = i18n.t('Wrong...');
					}

					self.collection.remove(options.model);
					window.localStorage.setItem(Settings.TMP_STORAGE_KEY, JSON.stringify(questionExplanation));
					window.AppRouter.goTo('trivia/' + groupId + '/' + options.data.question_pk + '/explenation');
				},
				error: function (xhr, status) {
					if (xhr.status === 403) 
					{
						window.AppAlert(i18n.t('Cannot answer a question that already been answered'), 
						null, i18n.t('Problem answering question'), i18n.t('OK'));
					}
				}
			});

			// console.debug('%o', options);
		},
	});

	var QuestionExplanationView = BaseView.extend({
		pageTitle: function () {
			if (Settings.I18n === 'he')
				return 'תוצאה';
			else
				return 'Answer Result';
		},

		templateSource: require('text!templates/trivia/question_explanation.html'),

		events: 
		{
			'click #continue': 'next'
		},

		initialize: function(options)
		{
			console.log('binding question explanation with: %o', options.model);
		},

		isReady: function ()
		{
			return (this.model.explanation !== undefined) && (this.model.explanation !== null);
			// return this.model.has('explanation');
		},

		render: function()
		{
			this.$el.html(this.renderTemplate({ questionExplanation: this.model }));
			return this;
		},

		next: function (event)
		{
			console.log('redirecting back to questionView');
			//TODO: if not redirecting back raise an event to continue the questions.
			Backbone.history.history.back();
		},
	});

	
	return {
		// GroupSelectionView: GroupSelectionView,
		GroupView: QuestionGroupView,
		QuestionView: QuestionView,
		QuestionExplanationView: QuestionExplanationView
	}
});