define([
	'app/settings', 'zepto', 'vendor/slide.zepto.min', 'backbone', 'handlebars',
	'app/views/base', 'vendor/snap.min',
	'app/models/user', 'app/models/navigation',
	'app/models/event', 'app/models/base'], 
function(Settings, $, $_Slide, Backbone, Handlebars, BaseView, Snap, UserModel, Navigation, EventModel, BaseModel)
{
	var sibebarViewModel = BaseModel.Model.extend({
		initialize: function(options) 
		{
			var self = this;
			window.EventUser.on('change:points', function (newUser) {
				self.set('points', newUser.get('points'));
			});
		}
	});

	var SIDEBAR_GAME_MENU_TEMPLATE = Handlebars.compile('\
		{{#each links}}									\
		<a class="item" href="#{{route}}"><i class="icon ion-paper-airplane"></i> {{label}}</a>	\
		{{/each}}										\
	');

	return BaseView.extend({
		el: 'aside#sidebar',

		direction: function() { return Settings.SNAP_OPTIONS.disable === 'right' ? 'left' : 'right'; },

		events: {
			'click a.item'				: 'close', // makes navigation links close the sidebar
			'click a.submenu-deploy'	: 'deploySubmenu'
		},

		ui: {
			'gameSubmenu': '#game-submenu-header'
		},

		bindings: {
			// 'a#event-title': 'title'
			'#attendee-info h1' : 'full_name', // AppGrade User
			'#attendee-score' : 'points', // Score: 0
			'#attendee-rank' : 'attendance_rank',
			'.sidebar-controls em strong' : 'aggregate_points' // Total Event Score
		},

		initialize: function(options) 
		{
			this.templateSource = this.$('script#sidebar-template').html(); // Fetch template from inline script tag
			this.$el.html(this.renderTemplate());
			this.bindUI();

			var snapper = new Snap(_.extend({element: options.contentEl}, Settings.SNAP_OPTIONS));
			this._snapper = snapper;
			var self = this;

			this._snapper.on('open', function () {
				window.AppVent.trigger('sidebar:refresh');
			});

			this._snapper.on('animating', function () {
				self.toggleOverlay(self);
			});

			console.debug('Initialized Snap with element', options.contentEl);

			window.AppVent.on('sidebar:disable', function(event) 
			{
				self._snapper.disable(); 
				self._snapper.close();
				self.unstickit(); // unstick the sidebar when its disabled
				window.AppVent.once('sidebar:bind', self.bindData, self); // relisten to bind event
			});

			window.AppVent.once('sidebar:bind', this.bindData, this);
			window.AppVent.on('sidebar:refresh', this.refreshUser, this);

			// Load game links
			this.gameLinkCollection = new Navigation.LinkCollection();
			this.gameLinkCollection.load(function(models)
			{
				// TODO: check if it's possible to use `models`
				var collection = self.gameLinkCollection.toJSON();
				$(self.ui.gameSubmenu).after(SIDEBAR_GAME_MENU_TEMPLATE({links: collection}));
			});
		},

		toggleOverlay: function(self) 
		{
			var dir = Settings.SNAP_OPTIONS.disable;
			var state = self._snapper.state().state;

			if (state === 'closed' || state === dir)
			{
				$('div.overlay').addClass('overlay-closed');
				$('div.overlay').css('display', 'none');
			}
			else
			{
				$('div.overlay').removeClass('overlay-closed');
				$('div.overlay').css('display', 'block');
			}
		},

		enable: function()
		{
			this._snapper.enable();
			$('button.ion-navicon').css('display', 'block');
		},

		close: function(event)
		{
			this._snapper.close();
		},

		open: function(event)
		{
			event.preventDefault();
			
			var dir = _.result(this, 'direction');

			if (this._snapper.state().state === dir)
				this._snapper.close();
			else
				this._snapper.open(dir);
		},

		deploySubmenu: function(event)
		{
			// console.debug('%o', $(this));
			$(event.currentTarget).parent().find('.nav-item-submenu').slideToggle();
			$(event.currentTarget).find('em').toggleClass('dropdown-nav');
		},

		bindData: function(event)
		{
			var curEvent = this.__getEvent();
			var viewModelAttributes = {};

			if (window.EventUser)
			{
				_.extend(viewModelAttributes, window.EventUser.attributes, { aggregate_points: curEvent.aggregate_points });
				this.model = new sibebarViewModel(viewModelAttributes);
				this.stickit();
			}
		},

		__getEvent: function ()
		{
			var eventId = window.localStorage.getItem(Settings.EVENT_ID_KEY);
			return JSON.parse(window.localStorage.getItem('MODEL_' + eventId));
		},

		refreshUser: function ()
		{
			window.AppVent.trigger('sidebar:bind');

			var eventObj = new EventModel();
			eventObj.update();
			window.EventUser.updatePoints();

			var self = this;
			eventObj.on('change', function ()
			{
				self.model.set('aggregate_points', eventObj.get('aggregate_points'));			
			});
		},
	});
});