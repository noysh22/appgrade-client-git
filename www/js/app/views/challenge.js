define(['app/settings', 'app/helpers/i18n', 'underscore', 'zepto', 'app/helpers/toggler','backbone', './base', 'app/models/challenge',
		'text!templates/challenges/challenge.html',
		'text!templates/challenges/challenge_session.html',
		'text!templates/challenges/challenge_result.html',
		'text!templates/challenges/game_over.html'], 
function(Settings, i18n, _, $, _ToggleHelper, Backbone, BaseView, ChallengeModels,
		CHALLENGE_TEMPLATE, CHALLENGE_SESSION_TEMPLATE, CHALLENGE_RESULT_TEMPLATE, GAME_OVER_TEMPLATE)
{
	'use strict';

	var CLASSIFICATION_LABELS = [
		'Challenge',
		'Networking',
		'Collectibles',
		'After Hours'
	];

	var GameOverView = BaseView.extend({
		templateSource: GAME_OVER_TEMPLATE,
		render: function() { this.$el.html(this.renderTemplate()); return this; }
	});

	var NextChallengeID = null;

	var challengeView = BaseView.extend({
		templateSource: CHALLENGE_TEMPLATE,

		imageUrl: null,

		ui: {
			'img': 'form #img-caption',
			'capturedImg': 'form #capture',
			'captureWrapper': 'form #capture-wrapper', 
			'camera': '.camera',
			'galleryBtn': '.gallery-btn',
			'textAnswer': '#text-answer',
			'answerBtn' : '#answer',
			'skipBtn' : '#skip-btn',
		},

		bindings: {
			'#userScore': 'points',
		},

		events: {
			'click #skip-btn': function(event)
			{
				event.preventDefault();
				this.trigger('challenge:skip');
			},

			'click .camera' : 'openCamera',
			'click .gallery-btn' : 'openGallery',

			'click #answer' : function(event)
			{
				event.preventDefault();

				var data = {};

				data.text_solution = $(this.ui.textAnswer).val();

				if(this.imageUrl)
					data.imageUrl = this.imageUrl;
				else if (data.text_solution == "" || 
					data.text_solution == undefined || 
					data.text_solution == null) 
				{
					window.AppAlert(i18n.t('Answer must have either a text or picutre'),
						null, i18n.t('Could not submit answer'), i18n.t('OK'));	
					return;
				};

				$(this.ui.answerBtn).toggleButton(i18n.t('Submitting...'));
				this.trigger('challenge:answer', { data: data, model: this.model });
			},
		},

		initialize: function (options)
		{
			console.log('initializing Challenge View with model:%o', options.model);

			if(navigator.camera) // in case camera plugin is present
				this.onCaptureSuccess = _.bind(this.onCaptureSuccess, this);
		},

		isReady: function ()
		{
			return this.model.has('title') && this.model.has('description');
		},

		toggleImage: function ()
		{
			// Hide image tag when there is no image in the question
			if (!this.model.has('image')) this.$(this.ui.img).hide();
			else 
			{
				this.ui.img.attr('src', Settings.SERVER_ROOT + this.model.get('image'));
				this.ui.img.show('fast');
			}
		},

		openCamera: function(event) 
		{
			event.preventDefault();

			navigator.camera.getPicture(this.onCaptureSuccess, this.onCaptureFail, 
				{ quality: 50, destinationType: Camera.DestinationType.FILE_URI });

			console.log('opening camera');
		},

		openGallery: function(event)
		{
			event.preventDefault();

			navigator.camera.getPicture(this.onCaptureSuccess, this.onCaptureFail, 
				{ quality: 50, sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,
					destinationType: Camera.DestinationType.FILE_URI });

			console.log('opening camera');
		},

		onCaptureSuccess: function(imageData)
		{
			this.imageUrl = imageData;
			$(this.ui.capturedImg).attr('src', this.imageUrl).css('display', 'block');
			$(this.ui.captureWrapper).addClass('show-capture');
		},

		onCaptureFail: function(message)
		{
			// alert("Error in capturing image:", message);
			console.log('Error in picture or camera operation was cancelled', message);
		},

		render: function ()
		{
			console.log('Rendering challenge', this.model);
			var modelData = null;
			if(this.model) 
				modelData = this.model.toJSON();
			else if(this.model == undefined)
				return;

			this.$el.html(this.renderTemplate({ challenge: modelData }));
			
			if(this.model)
			{
				this.bindUI();
				this.toggleImage();

				if(!navigator.camera) 
				{ // in case camera plugin is not present
					$(this.ui.camera).hide();
					$(this.ui.galleryBtn).hide();
				}

				if (window.EventUser !== null && window.EventUser !== undefined) 
				{
					this.stickit(window.EventUser);
				}	
			}

			return this;
		},
	});

	var challengesListView = BaseView.extend({

		rendered: false,

		nextChallengeID: 0, // this used to keep track on the next challenge which should be loaded

		pageTitle: function () {
			console.log('Getting title by by classification index', this.collection.classification);
			return i18n.l(CLASSIFICATION_LABELS[parseInt(this.collection.classification) - 1]);
		},

		templateSource: CHALLENGE_SESSION_TEMPLATE, // TODO: write the html view for this collection

		ui: {
			'challengeContainer': '#challenge-container',
			'answerBtn': '#answer',
			'subheader': '#challenge-subheader .title'
		},

		initialize: function(options)
		{
			console.log('initializing challengesListView with %o', options);

			this.collection = new ChallengeModels.ChallengeCollection(null, {classification: options.classification });
			this.challengeView = null;

			if (options.next) 
			{
				this.nextChallengeID = parseInt(options.next);
			}
			
			this.skipChallenge = _.bind(this.skipChallenge, this);
			this.answerChallenge = _.bind(this.answerChallenge, this);

			this.gameOverView = new GameOverView();
			// this.answerChallengeWithImage = _.bind(this.answerChallengeWithImage, this);
			
			var self = this;
			this.collection.fetch({	reset: true	});

			// after the collection is fetched its being reset, and them a filter can be apllied
			this.collection.once('reset', function (collection) 
			{
				self.collection.set( collection.where({ can_be_submitted: true }) );

				if (self.collection.models.length > 0) 
				{
					self.updateChallenge();		
				};
				
				self.trigger('ready');
			});
		},

		isReady: function()
		{
			console.log('Delegating isReady() to child challenge view', this.challengeView);
			return this.challengeView && this.challengeView.isReady();
		},

		updateChallenge: function ()
		{
			console.debug('Updating challenge view; Previous:', this.challengeView);

			if (this.challengeView && this.challengeView.isDestroyed() === false) {
				console.debug('Destroying previous question view');
				this.challengeView.off('challenge:skip', this.skipChallenge); // TODO: make sure this is really needed
				this.challengeView.off('challenge:answer', this.answerChallenge);
				this.challengeView.destroy();
			}

			var chModel = this.collection.at(this.nextChallengeID);
			
			console.debug('Initializing challengeView with model', chModel);
			this.challengeView = new challengeView({ model: chModel });
			this.challengeView.on('challenge:skip', this.skipChallenge);
			this.challengeView.on('challenge:answer', this.answerChallenge);
			this.challengeView.render();

			this.injectNestedView(this.challengeView, this.ui.challengeContainer);

			console.debug('Element after rendering questionView:', this.challengeView.el);
		},

		handleChallengeAnswer: function(model, challengeResult) // calculated the next challenge
		{
			if (challengeResult.status == Settings.CHALLENGE_STATUS_REJECTED) 
			{
				
				this._setNextChallengeID(model);
			}
			else
			{
				this.collection.remove(model); // removing the challenge from the collection
				this.nextChallengeID = 0;
			}

			challengeResult.next = this.nextChallengeID; // set the return index for the next challenge

			this.updateChallenge();
		},

		_setNextChallengeID: function(model) // setting the next challenge index in the collection
		{
			var curChallengeIndex = this.collection.indexOf(model);

			if (curChallengeIndex + 1 == this.collection.models.length) 
			{
				this.nextChallengeID = 0;
			}
			else
			{
				this.nextChallengeID = curChallengeIndex + 1;	
			}
		},

		skipChallenge: function () // skip a challenge
		{
			console.log('skipping challenge:', this.collection.models[0]);
			if(this.collection.models.length > 1)
			{
				var curModel = this.collection.at(this.nextChallengeID);
				this._setNextChallengeID(curModel);
				this.updateChallenge();
			}
			else
			{
				window.AppAlert(i18n.t('No more challeges after this one, cannot skip this challenge'), 
						null, null, i18n.t('OK'));
			}
		},

		render: function()
		{
			if (!this.rendered) {
				console.debug('Rendering ChallengesView once');
				this.$el.html(this.renderTemplate());
				this.bindUI();
				this.rendered = true;
			}

			if (this.collection.models.length > 0)
			{
				this.injectNestedView(this.challengeView, this.ui.challengeContainer);

				if(this.collection.models.length == 1) this.challengeView.ui.skipBtn.hide();
			} 
			else 
			{
				this.injectNestedView(this.gameOverView, this.ui.challengeContainer);
			}

			return this;
		},

		_getChallengeResult: function(answerStatus)
		{
			var challengeResult = {};
	
			switch(answerStatus)
			{
				case Settings.CHALLENGE_STATUS_UNREVIEWED:
					challengeResult.status = Settings.CHALLENGE_STATUS_UNREVIEWED;
					challengeResult.resultClass = 'pending';
					challengeResult.result = i18n.t("Waiting for Review");
					challengeResult.explanation = i18n.t("Your image will be reviewed later, press Continue to keep playing");
					break;
				case Settings.CHALLENGE_STATUS_ACCEPTED:
					challengeResult.status = Settings.CHALLENGE_STATUS_ACCEPTED;
					challengeResult.resultClass = 'correct';
					challengeResult.result = i18n.t("Correct");
					challengeResult.explanation = i18n.t("Good job! press continue to move to the next challenge");

					window.EventUser.updatePoints();

					break;
				case Settings.CHALLENGE_STATUS_REJECTED:
					challengeResult.status = Settings.CHALLENGE_STATUS_REJECTED;
					challengeResult.resultClass = 'wrong';
					challengeResult.result = i18n.t("Wrong");
					challengeResult.explanation = i18n.t("Nice try, press the button below to continue");
					break;
			}

			return challengeResult;
		},

		answerChallenge: function(options)
		{

			options.data.attendance_pk = window.EventUser.get('attendance_pk');
			options.data.challenge = options.model.get('resource_uri');

			console.log('Answering challenge', options.model.get('id'));

			if (options.data.imageUrl)
			{
				this.answerChallengeWithImage(options);
			}
			else
			{
				var self = this;
				$.ajax({
					// url: Settings.resourceUriFor('challenge/' + options.model.get('id') + '/challenge-submission/'),
					url: Settings.resourceUriFor('challenge-submission/'),
					type: 'POST',
					data: JSON.stringify(options.data),
					success: function (data) 
					{	
						var challengeResult = self._getChallengeResult(data.status);

						self.handleChallengeAnswer(options.model, challengeResult);

						self.goToResultPage(options.model, challengeResult);
					}
				});
			}
		},

		answerChallengeWithImage: function(options)
		{

			var fileOptions = new FileUploadOptions();
			fileOptions.fileKey = 'image_solution';
			fileOptions.fileName = options.data.imageUrl.substr(options.data.imageUrl.lastIndexOf('/') + 1);
			fileOptions.mimeType = 'image/jpeg';
			fileOptions.chunkedMode = false; // prevents problems uploading to Nginx from Android
			fileOptions.headers = {'Authorization': $.ajaxSettings.headers.Authorization};

			fileOptions.params = {
				'attendance_pk': options.data.attendance_pk,
				'challenge': options.data.challenge
			};

			if (options.data.text_solution)
				fileOptions.params.text_solution = options.data.text_solution;

			var fileTransfer = new FileTransfer();
			var self = this;

			fileTransfer.upload(options.data.imageUrl, 
				Settings.resourceUriFor('challenge-submission/'),
				function(response)
				{
					var resObj = JSON.parse(response.response);
					var challengeResult = self._getChallengeResult(resObj.status);

					self.handleChallengeAnswer(options.model, challengeResult);

					self.goToResultPage(options.model, challengeResult);
				},
				function(error)
				{
					console.log(error);
					$(self.challengeView.ui.answerBtn).toggleButton(i18n.t('Answer'));
					window.AppAlert(i18n.t('Error occured while uploading image'), null, null, i18n.t('OK'));

				}, fileOptions);
		},

		goToResultPage: function(model, challengeResult) 
		{
			var baseChallengeUrl = 'challenge/' + model.get('classification') + '/'; 

			challengeResult.next = baseChallengeUrl + challengeResult.next;

			window.localStorage.setItem(Settings.TMP_STORAGE_KEY, JSON.stringify(challengeResult));

			$(this.challengeView.ui.answerBtn).toggleButton(i18n.t('Answer'));
			window.AppRouter.goTo(baseChallengeUrl + model.get('id') + '/result');
		},

	});

	var challengeResult = BaseView.extend({
		pageTitle: i18n.l('Answer Result'),

		templateSource: CHALLENGE_RESULT_TEMPLATE,

		events:
		{
			'click #continue' : 'next'
		},

		isReady: function()
		{
			return (this.model.result !== undefined) && (this.model.result !== null);
		},

		render: function()
		{
			this.$el.html(this.renderTemplate({ challengeResult: this.model }));
			return this;
		},	

		next: function (event)
		{
			event.preventDefault();
			console.log('redirecting back to questionView');
			window.AppRouter.goTo(this.model.next); // return to the next challenge
		},
	});

	return {
		ChallengeView: challengeView,
		ChallengesListView: challengesListView,
		ChallengeResult: challengeResult
	};

});