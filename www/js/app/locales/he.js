define({
	DIRECTIONALITY: 'rtl',
	
	'ANS BIRTHDAY': 'יומהולדת למחלקת ANS',
	'OK': 'אישור',
	'Total event score': 'ניקוד כולל',
	'Score': 'נקודות',
	'Rank': 'דירוג',
	'Navigation': 'תפריט ניווט',
	'Home': 'עמוד הבית',
	'Games': 'משחקים',
	'Trivia': 'טריויה',
	'Session Quest': 'סשיין קווסט',
	'Challenge': 'אתגרים',
	'Challenges': 'אתגרים',
	'Networking': 'נטוורקינג',
	'Collectible': 'אספנות',
	'After Hours': 'פעילות ערב',
	'Logout': 'התנתק',
	'Get social!': 'בואו נתחבר!',
	'Play': 'שחק',
	'Event code': 'קוד אירוע',
	'Full name': 'שם מלא',
	'Username': 'שם משתמש',
	'Email': 'דוא״ל',
	'Password': 'סיסמה',
	'Or': 'או',
	'Log In': 'התחבר',
	'Logging In': 'מתחבר',
	'Register': 'הירשם',
	'Registration code': 'קוד הרשמה',
	'Challenge your environment': 'אתגר את הסובבים אותך',
	'Test your knowledge': 'בחן את הידע שלך',

	'Sorry, this is the last question in this game.': 'מצטערים, זו השאלה האחרונה במשחק זה.',
	'No more challeges after this one, cannot skip this challenge' : 'מצטערים, זהו האתגא האחרון במשחק זה.',
	
	'Keep playing?': 'משחק אחר?',

	'Points for question': 'נקודות לשאלה',
	'Total': 'מצטבר למשחק',
	'Answer': 'שלח',
	'Skip': 'דלג',
	'We\'re all out of questions for this game!': 'נגמרו השאלות עבור המשחק!',
	'We\'re all out of challenges for this game!': 'נגמרו האתגרים עבור המשחק!',
	'Continue Playing': 'המשך לשחק',
	'Cannot navigate to this page without choosing an answer first': 'לא ניתן להגיע לעמוד זה ללא בחירת תשובה',
	'Points for challenge': 'נקודות לאתגר',
	'Take picture': 'צלם/י תמונה',
	'Choose picture': 'בחר/י תמונה',
	'Answer...': 'כתוב תשובתך...',

	'All rights reserved': 'כל הזכויות שמורות',

	'Submitting...': 'שולח...',

	'Waiting for Review': 'מחכה לבדיקה',
	'Your image will be reviewed later, press Continue to keep playing' : 'התמונה שלך תיבדק בהמשך, לחץ על הכפתור להמשך המשחק',
	'Correct' : 'נכון מאוד',
	'Good job! press continue to move to the next challenge' : 'עבודה מעולה! לחץ על המשך כדי לעבור לאתגר הבא',
	'Wrong...' : 'טעות',
	'Nice try, press the button below to continue' : 'ניסיון יפה, לחץ על המשך כדי להמשיך לשחק',

	'Leaderboard': 'לוח מובילים',

	'Required Field': 'שדה חובה',

	'Numbers only' : 'מספרים בלבד',

	'Letters only' : 'אותיות בלבד',

	'5-15 letters and numbers': '5-15 תווים ומספרים בלבד',

	'Wrong email': 'אימייל שגוי',

	'4-20 numbers' : '4-20 מספרים בלבד',

	'Failed to login' : 'התחברות נכשלה',

	'Incorrect username or password': 'שם משתמש או סיסמה שגויים',

	'Event Id is not correct' : 'מספר אירוע שגוי',

	'Cannot login, error occured during authentication' : 'שגיאה באימות נתונים מול השרת.',

	'The event is currently locked, could not login, please try again later.': 'האירוע סגור לשימוש, אנא נסה שנית מאוחר יותר.',

	'Event is locked!' : 'האירוע נעול!',

	'Press ok to continue' : 'לחץ אוקי כדי להמשיך',

	'Cannot answer a question that already been answered': 'לא ניתן לענות על שאלה שנענתה כבר',

	'Problem answering question' : 'שגיאה בשליחת תשובה',

	'Error occured while uploading image': 'העלאת תמונה נכשלה',

	'Congratulations this game is over!' : 'כל הכבוד, סיימת את המשחק',

	'Could not submit answer' : 'לא ניתן לעשלוח תשובה',

	'Answer must have either a text or picutre' : 'תשובה חייבת להכיל טקסט או תמונה'

});