define(function(require)
{
	'use strict';

	var $ = require('zepto'), _ = require('underscore'), Backbone = require('backbone'), Settings = require('app/settings');
	var LayoutView = require('app/views/layout'), LoginView = require('app/views/login'), EventView = require('app/views/event'),
	LeaderboardView = require('app/views/leaderboard'), i18n = require('app/helpers/i18n');
	
	window.goTo = function(url)
	{
		if (url != '#') url = '#' + url;
		console.log('Redirecting to: ' + url);
		return window.location.replace(url);
	}

	window.AppAlert = function(msg, callback, title, buttonName) 
	{
		if(!title) { title = i18n.t('Error'); }

		if (!buttonName) { buttonName = i18n.t('OK'); }

		if(navigator.notification)
		{
			navigator.notification.alert(msg, callback, title, buttonName);
		}
		else
		{
			alert(msg);
		}
	}
	
	return Backbone.Router.extend({
		initialize: function()
		{
			var self = this;
			$(document).on('ajaxComplete', function(e, xhr, options)
			{
				if (xhr.status === 401)
				{
					if (!window.location.hash.startsWith('#login'))
					{					
						console.log('Caught HTTP 401');
						return self.goTo('login/'+ escape(window.location.hash));											
					}
				}
			});
			
			this.layout = new LayoutView();
		},

		routes: {
			'': 'event',
			'register/': 'register',
			'login/': 'authenticate',
			'login/*redirectTo': 'authenticate',
			'trivia/:groupId': 'triviaQuestionGroup',
			'trivia/:groupId/:questionId/explenation': 'triviaQuestionExplanation',
			'challenge/:classification(/:next)': 'challenges',
			'challenge/:classification/:challengeId/result': 'challengeResult',
			'leaderboard': 'leaderboard',
			'logout': 'logout',
		},

		/**
		 * Navigates to the specified route, optionally without pushing a new history state.
		 * @param  {string} route		Route to navigate to.
		 * @param  {boolean} replace	True to replace current history state, false (default) to push a new one.
		 */
		goTo: function(route, replace)
		{
			console.log('Redirecting to '+ route);
			if (_.isUndefined(replace))
				replace = false;
			return this.navigate(route, {trigger: true, replace: replace});
		},

		ensureAuthentication: function()
		{
			if (!this.isAuthenticated())
			{
				console.warn('Not authenticated');
				this.goTo('login/'+ location.hash.replace('#', ''), true);
				return false;
			}
			return true;
		},

		isAuthenticated: function()
		{
			return $.ajaxSettings !== undefined
				&& $.ajaxSettings.headers !== undefined
				&& 'Authorization' in $.ajaxSettings.headers
				&& window.localStorage.getItem(Settings.EVENT_ID_KEY) !== null;
		},

		event: function()
		{
			if (this.ensureAuthentication() === true)
				this.layout.setMainView(new EventView({eventId: window.localStorage.getItem(Settings.EVENT_ID_KEY)}));
		},

		authenticate: function(redirectTo)
		{
			if (!redirectTo) redirectTo = '';
			console.log('In authenticate() route');
			this.layout.setMainView(new LoginView({redirectTo: unescape(redirectTo)}));
		},

		register: function()
		{
			var self = this;
			require(['app/views/register'], function(RegisterView) 
			{
				var view = new RegisterView();		
				self.layout.setMainView(view);
			});
		},

		triviaQuestionGroup: function(groupId)
		{
			var self = this;
			require(['app/views/trivia'], function(TriviaViews)
			{
				var qGroup = new TriviaViews.GroupView({questionGroupId: groupId}); // TODO: DRY the URI
				self.layout.setMainView(qGroup);
			});
		},

		triviaQuestionExplanation: function(groupId, questionId)
		{
			var explanation = JSON.parse(window.localStorage.getItem(Settings.TMP_STORAGE_KEY));
			var self = this;
			require(['app/views/trivia'], function(TriviaViews)
			{
				var explenationView = new TriviaViews.QuestionExplanationView({model: explanation});
				self.layout.setMainView(explenationView);
			});
		},

		challenges: function(classification, next)
		{
			var ChallengeViews = require('app/views/challenge');
			var challengeListView = new ChallengeViews.ChallengesListView({classification: classification, next: next});

			this.layout.setMainView(challengeListView);
		},

		challengeResult: function(classification, challengeId)
		{
			var result = JSON.parse(window.localStorage.getItem(Settings.TMP_STORAGE_KEY));
			var self = this;

			require(['app/views/challenge'], function(ChallengeViews)
			{
				var challengeResultView = new ChallengeViews.ChallengeResult({ model: result });
				self.layout.setMainView(challengeResultView);
			});
		},

		leaderboard: function()
		{
			var leaderboardView = new LeaderboardView();

			this.layout.setMainView(leaderboardView);
		},

		logout: function() 
		{
			this.layout.trigger('loading');
			window.AppVent.trigger('sidebar:disable');
			window.localStorage.clear();
			window.sessionStorage.clear();
			self.goTo('login/', true);	
		}
	});
});