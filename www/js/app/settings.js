define(function()
{
	var serverUrl = 'http://li269-152.members.linode.com';
	var NOTIFICATION_PORT = 81;

	return {
		'SERVER_ROOT': serverUrl,
		'API_ENDPOINT': serverUrl,
		resourceUriFor: function(resourceName) { return serverUrl + '/api/v1/' + resourceName; },

		'NOTIFICATION_ENDPOINT': serverUrl + ':' + NOTIFICATION_PORT,

		// Display options for spin.js
		'SPIN_OPTIONS': {
			lines: 13, // The number of lines to draw
			length: 20, // The length of each line
			width: 10, // The line thickness
			radius: 30, // The radius of the inner circle
			corners: 0.8, // Corner roundness (0..1)
			rotate: 0, // The rotation offset
			direction: 1, // 1: clockwise, -1: counterclockwise
			color: '#000', // #rgb or #rrggbb or array of colors
			speed: 1.1, // Rounds per second
			trail: 60, // Afterglow percentage
			shadow: false, // Whether to render a shadow
			hwaccel: true, // Whether to use hardware acceleration
			className: 'activity-spinner', // The CSS class to assign to the spinner
			zIndex: 200000, // The z-index (defaults to 2000000000)
			top: '30%', // Top position relative to parent
			left: '50%' // Left position relative to parent
		},

		'SNAP_POSITION': 282,
		'SNAP_OPTIONS': {
			hyperextensible: false,
			easing: 'ease-in-out',
			maxPosition: 0,
			minPosition: 0,
		},

		'TMP_STORAGE_KEY': 'tmp',

		'EVENT_ID_KEY': 'EVENT_ID',
		'EVENT_URI_ID_INDEX': 4,

		'CHALLENGE_STATUS_UNREVIEWED': 0,
		'CHALLENGE_STATUS_ACCEPTED': 1,
		'CHALLENGE_STATUS_REJECTED': 2,

		'LOCALE_PATH': 'app/locales',
		
		// Maps available UI languages to their ISO 639-1 code
		'AVAILABLE_UI_LANGUAGES': {
			'English': 'en',
			'Hebrew': 'he'
		},
		'DEFAULT_UI_LANGUAGE': 'en',
		'UI_LANGUAGE': 'he'
	};
});