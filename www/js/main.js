require.config({
	enforceDefine: false,
	baseUrl: 'js/',
	map: {'*': {
		'text': 'vendor/require.text',
		'underscore': 'vendor/underscore-min',
		'translate': 'app/helpers/i18n',
		'zepto': 'vendor/zepto.min',
		'backbone': 'vendor/backbone-min',
		'handlebars': 'vendor/handlebars-v2.0.0',
		'backbone.stickit.min': 'vendor/backbone.stickit.min'
	}},
	shim: {
		'vendor/underscore-min': { exports: '_' },
		'vendor/zepto.min': { exports: '$' },
		'vendor/slide.zepto.min': { deps: ['zepto'] },
		'vendor/backbone-min': { deps: ['underscore', 'zepto'], exports: 'Backbone' },
		'vendor/handlebars-v2.0.0': { exports: 'Handlebars' },
		'vendor/snap.min': { exports: 'Snap' }
	}
});


require(['require', 'app/settings', 'app/helpers/string', 'app/helpers/path', 'zepto', 'backbone',
	'vendor/fastclick', 'app/router', 'vendor/socket.io', 'app/helpers/i18n'],
function(require, Settings, StringHelper, Path, $, Backbone, FastClick, Router, io, i18n)
{
	function getLocaleModule()
	{
		if (navigator.globalization)
		{
			navigator.globalization.getPreferredLanguage(
				function (lang)
				{
					console.log('Navigator language:', lang);
					if (lang.value in Settings.AVAILABLE_UI_LANGUAGES)
						Settings.UI_LANGUAGE = Settings.AVAILABLE_UI_LANGUAGES[lang.value];
				},
				function () { alert('Error getting language'); }
			);
		}
		else if (navigator.language)
		{
			var locale = navigator.language;
			if (locale in Settings.AVAILABLE_UI_LANGUAGES)
				Settings.UI_LANGUAGE = locale.split('-');
		}

		return Path.join(Settings.LOCALE_PATH, Settings.UI_LANGUAGE);
	}
	
	function init(isBrowser) 
	{
		console.debug('init('+ isBrowser +')');

		// Hides splash screen when app is loaded
		if (!isBrowser)
			cordova.exec(null, null, 'SplashScreen', 'hide', []);

		$.ajaxSettings.contentType = 'application/json';

		require([getLocaleModule()], function(locale)
		{
			if (locale.DIRECTIONALITY)
			{
				$(document.body).addClass(locale.DIRECTIONALITY);

				if (locale.DIRECTIONALITY == 'rtl')
				{
					Settings.SNAP_OPTIONS.disable = 'left';
					Settings.SNAP_OPTIONS.minPosition = -1*Settings.SNAP_POSITION;
				}
				else if (locale.DIRECTIONALITY == 'ltr')
				{
					Settings.SNAP_OPTIONS.disable = 'right';
					Settings.SNAP_OPTIONS.maxPosition = Settings.SNAP_POSITION;
				}
			}

			window.AppVent = _.extend({}, Backbone.Events);
			window.AppRouter = new Router();

			Backbone.history.on('route', function()
			{
				if (window.AppRouter.isAuthenticated() == false)
					return;

				if (window.sessionStorage.getItem('EVENT_ID') === null)
					return;
				
				if (window.NotificationSocket)
					return;

				var ns = window.NotificationSocket = io.connect(Settings.NOTIFICATION_ENDPOINT);

				ns.on('connect', function()
				{
					var room = 'event_{0}'.format(window.sessionStorage.getItem('EVENT_ID'));
					ns.emit('join', room);
					console.log('Notification socket requested to join room "%s"', room);
				});
				
				ns.on('notification', function(notificationModel)
				{
					window.AppAlert(notificationModel.title, 
						null, notificationModel.descriptio, i18n.t('OK'));
				});

				ns.on('lock', function(context)
				{
					window.AppAlert(i18n.t('Event is locked!'), 
						null, i18n.t('Press ok to continue'), i18n.t('OK'));
					window.AppRouter.goTo('#logout');
				});
			});

			Backbone.history.start();
		});

		FastClick.attach(document.body);
	}

	if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/))
		document.addEventListener('deviceready', init, false);
	else init(true);
});